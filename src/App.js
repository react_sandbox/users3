import React from "react";
import { Switch, Route } from "react-router-dom";

import Navbar from "./components/Navbar/Navbar";
import Home from "./views/Home/Home";
import Users from "./views/Users/Users";
import Me from "./views/Me/Me";

import "./App.css";

function App() {
    return (
        <div className="App">
            <Navbar />
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route exact path="/users">
                    <React.Suspense fallback="Loading users...">
                        <Users />
                    </React.Suspense>
                </Route>
                <Route exact path="/me">
                    <Me />
                </Route>
            </Switch>
        </div>
    );
}

export default App;
