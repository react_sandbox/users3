export default (state, action) => {
    switch (action.type) {
        case "USERS_FETCH_SUCCEEDED":
            return action.users;
        case "USERS_FETCH_FAILED":
        default:
            return state;
    }
};
