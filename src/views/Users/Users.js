import React from "react";
import axios from "axios";
import { useRecoilValue, selector } from "recoil";

import styles from "./Users.module.scss";

import User from "./components/User/User";

const usersState = selector({
    key: "users",
    get: async ({ get }) => {
        const res = await axios.get(
            "https://jsonplaceholder.typicode.com/users"
        );
        if (res.status === 200) return res.data;
        else return ["Failed to load users"];
    }
});

const Users = props => {
    const users = useRecoilValue(usersState);

    return (
        <div className={styles.users}>
            {users && users.map(u => <User {...u} key={u.id} />)}
        </div>
    );
};

export default Users;
